##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=robot-experiments
ConfigurationName      :=Debug
WorkspacePath          := "C:\Work\Dev\orx-projects\robot-experiments\build\windows\codelite"
ProjectPath            := "C:\Work\Dev\orx-projects\robot-experiments\build\windows\codelite"
IntermediateDirectory  :=$(ConfigurationName)
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=sausage
Date                   :=22/07/2018
CodeLitePath           :="C:\Program Files\CodeLite"
LinkerName             :=C:/MinGW-5.3.0/bin/g++.exe
SharedObjectLinkerName :=C:/MinGW-5.3.0/bin/g++.exe -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=../../../bin/robot-experimentsd.exe
Preprocessors          :=$(PreprocessorSwitch)__orxDEBUG__ 
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :="robot-experiments.txt"
PCHCompileFlags        :=
MakeDirCommand         :=makedir
RcCmpOptions           := 
RcCompilerName         :=C:/MinGW-5.3.0/bin/windres.exe
LinkOptions            :=  
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch)../../../include/Scroll $(IncludeSwitch)$(ORX)/include 
IncludePCH             := 
RcIncludePath          := 
Libs                   := $(LibrarySwitch)orxd 
ArLibs                 :=  "orxd" 
LibPath                := $(LibraryPathSwitch). $(LibraryPathSwitch)../../../lib $(LibraryPathSwitch)$(ORX)/lib/dynamic $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := C:/MinGW-5.3.0/bin/ar.exe rcu
CXX      := C:/MinGW-5.3.0/bin/g++.exe
CC       := C:/MinGW-5.3.0/bin/gcc.exe
CXXFLAGS :=  -ffast-math -g -msse2 -fno-exceptions $(Preprocessors)
CFLAGS   :=  -ffast-math -g -msse2 -fno-exceptions $(Preprocessors)
ASFLAGS  := 
AS       := C:/MinGW-5.3.0/bin/as.exe


##
## User defined environment variables
##
CodeLiteDir:=C:\Program Files (x86)\CodeLite
Objects0=$(IntermediateDirectory)/src_robot-experiments.cpp$(ObjectSuffix) $(IntermediateDirectory)/src_robot.cpp$(ObjectSuffix) $(IntermediateDirectory)/src_weapon.cpp$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

PostBuild:
	@echo Executing Post Build commands ...
	cmd /c if exist $(ORX)\lib\dynamic\orx.dll copy /Y $(ORX)\lib\dynamic\orx*.dll ..\..\..\bin
	@echo Done

MakeIntermediateDirs:
	@$(MakeDirCommand) "$(ConfigurationName)"


$(IntermediateDirectory)/.d:
	@$(MakeDirCommand) "$(ConfigurationName)"

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/src_robot-experiments.cpp$(ObjectSuffix): ../../../src/robot-experiments.cpp $(IntermediateDirectory)/src_robot-experiments.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Dev/orx-projects/robot-experiments/src/robot-experiments.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_robot-experiments.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_robot-experiments.cpp$(DependSuffix): ../../../src/robot-experiments.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_robot-experiments.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_robot-experiments.cpp$(DependSuffix) -MM "../../../src/robot-experiments.cpp"

$(IntermediateDirectory)/src_robot-experiments.cpp$(PreprocessSuffix): ../../../src/robot-experiments.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_robot-experiments.cpp$(PreprocessSuffix) "../../../src/robot-experiments.cpp"

$(IntermediateDirectory)/src_robot.cpp$(ObjectSuffix): ../../../src/robot.cpp $(IntermediateDirectory)/src_robot.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Dev/orx-projects/robot-experiments/src/robot.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_robot.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_robot.cpp$(DependSuffix): ../../../src/robot.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_robot.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_robot.cpp$(DependSuffix) -MM "../../../src/robot.cpp"

$(IntermediateDirectory)/src_robot.cpp$(PreprocessSuffix): ../../../src/robot.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_robot.cpp$(PreprocessSuffix) "../../../src/robot.cpp"

$(IntermediateDirectory)/src_weapon.cpp$(ObjectSuffix): ../../../src/weapon.cpp $(IntermediateDirectory)/src_weapon.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Dev/orx-projects/robot-experiments/src/weapon.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_weapon.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_weapon.cpp$(DependSuffix): ../../../src/weapon.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_weapon.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_weapon.cpp$(DependSuffix) -MM "../../../src/weapon.cpp"

$(IntermediateDirectory)/src_weapon.cpp$(PreprocessSuffix): ../../../src/weapon.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_weapon.cpp$(PreprocessSuffix) "../../../src/weapon.cpp"


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r $(ConfigurationName)/


