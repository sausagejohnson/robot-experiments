#define __SCROLL_IMPL__
#include "robot-experiments.h"
#include "robot.h"
#include "weapon.h"
#undef __SCROLL_IMPL__


orxOBJECT *debugSpot;
orxOBJECT *stoppingBar;





void RobotExperiments::BindObjects()
{
	ScrollBindObject<Robot>("Robot");
	ScrollBindObject<Weapon>("WeaponObject");
}
void RobotExperiments::PostDebugSpot(orxVECTOR position) {
	position.fZ = -0.1;
	orxObject_SetPosition(debugSpot, &position);
}

orxSTATUS orxFASTCALL PhysicsEventHandler(const orxEVENT *_pstEvent)
{
	if (_pstEvent->eID == orxPHYSICS_EVENT_CONTACT_ADD) {
		orxOBJECT *pstRecipientObject, *pstSenderObject;

		orxPHYSICS_EVENT_PAYLOAD *pstPayload;
		pstPayload = (orxPHYSICS_EVENT_PAYLOAD *)_pstEvent->pstPayload;

		pstSenderObject = orxOBJECT(_pstEvent->hSender);
		pstRecipientObject = orxOBJECT(_pstEvent->hRecipient);

		orxSTRING senderObjectName = (orxSTRING)orxObject_GetName(pstSenderObject);
		orxSTRING recipientObjectName = (orxSTRING)orxObject_GetName(pstRecipientObject);

		if (orxString_Compare(senderObjectName, "WeaponObject") == 0) {

			//orxVector_Mulf(&pstPayload->vNormal, &pstPayload->vNormal, 500);
			orxFLOAT fRad = orxVector_FromCartesianToSpherical(&pstPayload->vNormal, &pstPayload->vNormal)->fTheta;

			orxVECTOR spreadLeft = orxVECTOR_0;
			orxVECTOR spreadRight = orxVECTOR_0;
			orxVector_2DRotate(&spreadLeft, &spreadLeft, orxMATH_KF_DEG_TO_RAD * -fRad);
			orxVector_2DRotate(&spreadRight, &spreadRight, orxMATH_KF_DEG_TO_RAD * fRad);

			//orxConfig_PushSection("SparkObject");
			//orxConfig_SetVector("Speed", &pstPayload->vNormal);
			//orxConfig_PopSection();


			orxOBJECT *sparks = orxObject_CreateFromConfig("Sparks");
			//orxObject_GetPosition(sparks, &sparksPosition);

			//rpmCurrent = 10;
			//orxObject_SetAngularVelocity (weapon, rpmCurrent);
			//orxObject_ApplyTorque(weapon, rpmCurrent);

			orxObject_SetPosition(sparks, &pstPayload->vPosition);
		}

		if (orxString_Compare(recipientObjectName, "WeaponObject") == 0) {
			//do something
		}
	}
	return orxSTATUS_SUCCESS;
}


orxOBJECT* RobotExperiments::GetChildObjectByName(orxOBJECT *parentObject, orxSTRING childName) {
	for (orxOBJECT *pstChild = orxObject_GetOwnedChild(parentObject);
		pstChild;
		pstChild = orxObject_GetOwnedSibling(pstChild))
	{
		const orxSTRING name = orxObject_GetName(pstChild);
		if (orxString_Compare(name, childName) == 0) {
			return pstChild;
		}
	}

	return orxNULL;
}

ScrollObject* RobotExperiments::GetChildScrollObjectByName(ScrollObject *parentObject, const orxSTRING childName) {
 
	for (ScrollObject *child = parentObject->GetOwnedChild(); child; child = child->GetOwnedSibling()) {
		if (orxString_Compare(child->GetModelName(), childName) == 0) {
			return child;
		}
	}
	
	return orxNULL;
}

orxSTATUS RobotExperiments::Init ()
{
    orxSTATUS result = orxSTATUS_SUCCESS;
 
	/* Creates the object */
	//object = orxObject_CreateFromConfig("Robot");
	orxObject_CreateFromConfig("Opponent");
	orxObject_CreateFromConfig("Walls");
	debugSpot = orxObject_CreateFromConfig("DebugSpot");


	
	ScrollObject *robot = CreateObject("Robot");
 
    return result;
}


/** Run function, is called every clock cycle
 */
orxSTATUS RobotExperiments::Run ()
{
    orxSTATUS result = orxSTATUS_SUCCESS;
 
	orxVECTOR mousePosition = orxVECTOR_0;

	orxMouse_GetPosition(&mousePosition);
	orxRender_GetWorldPosition(&mousePosition, orxNULL, &mousePosition);

	mousePosition.fZ = 0;

	//orxObject_SetPosition(stoppingBar, &mousePosition);

    /* Should quit? */
    if(orxInput_IsActive("Quit"))
    {
        /* Updates result */
        result = orxSTATUS_FAILURE;
    }

	//DoInputs();
 
    return result;
}
 
 
void RobotExperiments::Exit ()
{
}

orxSTATUS RobotExperiments::Bootstrap() const
{
	orxSTATUS eResult = orxSTATUS_SUCCESS;
	orxResource_AddStorage(orxCONFIG_KZ_RESOURCE_GROUP, "../data/config", orxFALSE);
	return eResult;
}

/** Main function
 */
int main(int argc, char **argv)
{
    /* Sets bootstrap function to provide at least one resource storage before loading any config files */
    //orxConfig_SetBootstrap(Bootstrap);

    /* Executes a new instance of tutorial */
    //orx_Execute(argc, argv, Init, Run, Exit);
	RobotExperiments::GetInstance().Execute (argc, argv);
    /* Done! */
    return EXIT_SUCCESS;
}
