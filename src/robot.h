#ifndef ROBOT_H
#define ROBOT_H

#include "robot-experiments.h"
#include "weapon.h"

//! MyGame class
class Robot : public ScrollObject
{
public:

private:
	//orxFLOAT movementSpeed;

	//const orxFLOAT POWER_MAX = 30;
	//const orxFLOAT MOTOR_POWER_STEPS = 0.5;
	const orxFLOAT MAX_WHEEL_POWER = 32;
	const orxFLOAT HALF_WHEEL_POWER = 16;

	//int pulseMarker = 0;
	//const int pulseFire = 4;

	orxOBJECT *debugSpot;
	//orxOBJECT *weapon;
	Weapon *weapon;

	virtual void    OnCreate();
	virtual void    OnDelete();
	virtual orxBOOL OnCollide(ScrollObject *_poCollider,
		const orxSTRING _zPartName,
		const orxSTRING _zColliderPartName,
		const orxVECTOR &_rvPosition,
		const orxVECTOR &_rvNormal);
	virtual void    Update(const orxCLOCK_INFO &_rstInfo);
	virtual void BodyThrust(orxVECTOR localInfluencePoint, orxFLOAT power);
	//virtual void orxFASTCALL PulseWeaponMotor();
};

#endif // ROBOT_H

