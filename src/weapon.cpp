#include "weapon.h"

void orxFASTCALL Weapon::PulseWeaponMotor(){
	
	orxOBJECT *weapon = this->GetOrxObject();
	orxFLOAT power = orxObject_GetAngularVelocity(weapon);

	//orxLOG("lerp %f", power / POWER_MAX);
	
	if (power < POWER_MAX){
		//orxLOG("power: %f", power);
		power += MOTOR_POWER_STEPS;
		//orxFLOAT smoothPower = orxMath_SmoothStep(0, POWER_MAX, power);
		
/*		for (orxFLOAT x=0; x<60; x++){
			orxFLOAT smooth = orxMath_SmoothStep(0, 60, x);
			orxLOG("x=%f Smooth=%f new %f", x, smooth, 60*smooth);
		}*/
		
		orxObject_SetAngularVelocity (weapon, power);

		orxBODY *body = orxOBJECT_GET_STRUCTURE(weapon, BODY);
		
		//orxLOG("smoothPower is now: %f", smoothPower);
	} else {
		//orxLOG("SPD is now: %f", orxObject_GetAngularVelocity(weapon));		
	}
	
	//orxLOG("SPD is now: %f", orxObject_GetAngularVelocity(weapon));	

}

void Weapon::OnCreate()
{
	int here = 1;

}



void Weapon::OnDelete()
{
	// Do nothing when deleted
}

void Weapon::SwitchOn(){
	onOffSwitch = orxTRUE;
}
void Weapon::SwitchOff(){
	onOffSwitch = orxFALSE;
}

void Weapon::Update(const orxCLOCK_INFO &_rstInfo)
{
/*	if (RobotExperiments::GetInstance().IsGamePaused() == orxFALSE) {
		return;
	}*/
	
	//Only pulse the motor when the marker cycles to the fire position
	if (onOffSwitch == orxTRUE){
		if (pulseMarker == pulseFire){
			PulseWeaponMotor();
			pulseMarker = 0;
		}
		pulseMarker++;
	}

	orxOBJECT *weapon = this->GetOrxObject();
	orxFLOAT power = orxObject_GetAngularVelocity(weapon);
	if (power > 0) {
		orxObject_SetVolume(weapon, (power / POWER_MAX));
		orxFLOAT pitch = (power / POWER_MAX) + 0.5;
		orxObject_SetPitch(weapon, pitch);		
	}


	if (orxInput_HasBeenActivated("CheckWeaponSpeed")){
		orxFLOAT power = orxObject_GetAngularVelocity(this->GetOrxObject());
		orxLOG("Current Weapon Speed: %f", power);
	}

}

orxBOOL Weapon::OnCollide(ScrollObject *_poCollider, 
	const orxSTRING _zPartName, 
	const orxSTRING _zColliderPartName, 
	const orxVECTOR &_rvPosition, 
	const orxVECTOR &_rvNormal)
{
	if (_poCollider == orxNULL) {
		return orxTRUE;
	}
	
	orxVECTOR vector1 = orxVECTOR_0;
	orxVECTOR vector2 = orxVECTOR_0;
	_poCollider->GetSpeed(vector1, orxFALSE);
	this->GetSpeed(vector2, orxFALSE);

	orxFLOAT speed1 = orxVector_GetSize(&vector1);
	orxFLOAT speed2 = orxVector_GetSize(&vector2);

	orxFLOAT totalEnergy = speed1+speed2;

	orxLOG("speeds: %f %f total: %f", speed1, speed2, speed1+speed2);

	
	
	//orxLOG("normal %f %f", _rvNormal.fX, _rvNormal.fY);
	const orxSTRING colliderName = _poCollider->GetModelName();
	if (
			orxString_SearchString(colliderName, "EnemyBodyPart") != orxNULL ||
			orxString_SearchString(colliderName, "EnemyWeapon") != orxNULL ||
			orxString_SearchString(colliderName, "Opponent") != orxNULL 
			
			
		)
		{
			orxVECTOR n = {0,0,0};
			orxFLOAT fRad = orxVector_FromCartesianToSpherical(&n, &_rvNormal)->fTheta;
/*
			orxVECTOR spreadLeft = orxVECTOR_0;
			orxVECTOR spreadRight = orxVECTOR_0;
			orxVector_2DRotate(&spreadLeft, &spreadLeft, orxMATH_KF_DEG_TO_RAD * -fRad);
			orxVector_2DRotate(&spreadRight, &spreadRight, orxMATH_KF_DEG_TO_RAD * fRad);*/

			//orxConfig_PushSection("SparkObject");
			//orxConfig_SetVector("Speed", &pstPayload->vNormal);
			//orxConfig_PopSection();

			orxFLOAT currentSpinPower = orxObject_GetAngularVelocity(this->GetOrxObject());
			
			if (currentSpinPower > 15){
				if (totalEnergy > 500){
					orxVECTOR sparkVector = {totalEnergy/2, 0, 0};
					orxConfig_PushSection("SparkObject");
					orxConfig_SetVector("Speed", &sparkVector);
					orxConfig_PopSection();
					
					orxOBJECT *sparks = orxObject_CreateFromConfig("Sparks");	
					orxObject_SetPosition(sparks, &_rvPosition);			
				}
				
				if (totalEnergy > 0 && totalEnergy < 300){
					this->AddSound("WeaponBiteSound");			
				}
				
				if (totalEnergy >= 300 && totalEnergy < 1000){
					this->AddSound("Clash01Sound");			
				}
				
				if (totalEnergy >= 1000 && totalEnergy < 2000){
					this->AddSound("Clash02Sound");			
				}
				
				if (totalEnergy >= 2000){
					this->AddSound("Clash03Sound");			
				}
			}
			

			//orxObject_GetPosition(sparks, &sparksPosition);
			//orxObject_SetRotation(sparks, 45);

			//rpmCurrent = 10;
			//orxObject_SetAngularVelocity (weapon, rpmCurrent);
			//orxObject_ApplyTorque(weapon, rpmCurrent);

			

	}


	return orxTRUE;
}


