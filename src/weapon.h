#ifndef WEAPON_H
#define WEAPON_H

#include "robot-experiments.h"

//! MyGame class
class Weapon : public ScrollObject
{
public:
	virtual void SwitchOn();
	virtual void SwitchOff();

private:

	const orxFLOAT POWER_MAX = 60;
	const orxFLOAT MOTOR_POWER_STEPS = 0.9;

	int pulseMarker = 0;
	const int pulseFire = 4;
	
	orxBOOL onOffSwitch = orxFALSE;

	virtual void    OnCreate();
	virtual void    OnDelete();
	virtual orxBOOL OnCollide(ScrollObject *_poCollider,
		const orxSTRING _zPartName,
		const orxSTRING _zColliderPartName,
		const orxVECTOR &_rvPosition,
		const orxVECTOR &_rvNormal);
	virtual void    Update(const orxCLOCK_INFO &_rstInfo);
	virtual void orxFASTCALL PulseWeaponMotor();


};

#endif // WEAPON_H

