#ifndef __ROBOTEXPERIMENTS_H_
#define __ROBOTEXPERIMENTS_H_
 
//! Includes
// The following define skips compilation of ScrollEd (map editor) for now
#define __NO_SCROLLED__
#include "Scroll.h"
 
//! RobotExperiments class
class RobotExperiments : public Scroll<RobotExperiments>
{
public:
 	virtual orxOBJECT* GetChildObjectByName(orxOBJECT *parentObject, orxSTRING childName);
	virtual ScrollObject* GetChildScrollObjectByName(ScrollObject *parentObject, const orxSTRING childName);

private:
    //! Initialize the program
    virtual orxSTATUS Init ();
    //! Callback called every frame
    virtual orxSTATUS Run ();
    //! Exit the program
    virtual void      Exit ();
	
	virtual orxSTATUS Bootstrap() const;
	
	virtual void PostDebugSpot(orxVECTOR position);
	virtual void BindObjects();

	
};
 
#endif // __ROBOTEXPERIMENTS_H_