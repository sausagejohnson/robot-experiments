#include "robot.h"

//orxOBJECT *lookAheadTest;
//orxOBJECT *chassis;



void Robot::OnCreate()
{
	//chassis = orxObject_CreateFromConfig("Robot");
	//leftWheel = RobotExperiments::GetInstance().GetChildObjectByName(chassis, "LeftWheel");
	//rightWheel = RobotExperiments::GetInstance().GetChildObjectByName(chassis, "RightWheel");
	weapon = (Weapon *)RobotExperiments::GetInstance().GetChildScrollObjectByName(this, "WeaponObject");
	
	int here = 1;
	

}

void Robot::BodyThrust(orxVECTOR localInfluencePoint, orxFLOAT power) {
	//orxLOG("THrust");
	orxFLOAT FORCE = power;
	orxVECTOR thrust = { 0, -FORCE, 0 };

	orxVECTOR thrustDirection = { 0,0,0 };
	orxVECTOR thrustPosition = orxVECTOR_0;

	orxVECTOR objectPosition = orxVECTOR_0;
	this->GetPosition(objectPosition, orxFALSE);

	orxFLOAT objectRadians = this->GetRotation(orxFALSE);
	orxFLOAT facingRotation = (orxMATH_KF_RAD_TO_DEG * objectRadians) + 0;

	orxVector_2DRotate(&thrustDirection, &thrust, objectRadians);

	orxVector_2DRotate(&localInfluencePoint, &localInfluencePoint, objectRadians);

	orxVector_Add(&thrustPosition, &objectPosition, &localInfluencePoint);

	orxOBJECT *chassis = this->GetOrxObject();
	orxObject_ApplyForce(chassis, &thrustDirection, &thrustPosition);
	//PostDebugSpot(thrustPosition);

	//orxLOG("rot:%f x:%f y:%f", facingRotation, thrustPosition.fX, thrustPosition.fY);

}



void Robot::OnDelete()
{
	// Do nothing when deleted
}


void Robot::Update(const orxCLOCK_INFO &_rstInfo)
{

	orxFLOAT leftWheelPower = 0;
	orxFLOAT rightWheelPower = 0;

	orxBOOL controlActive = orxFALSE;

	if (orxInput_HasBeenActivated("WeaponOn")){
		weapon->SwitchOn();
	}
	
	if (orxInput_HasBeenActivated("WeaponOff")){
		weapon->SwitchOff();
	}

	if (orxInput_IsActive("StickLeft") &&
		!orxInput_IsActive("StickRight") &&
		!orxInput_IsActive("StickForward") &&
		!orxInput_IsActive("StickBackwards")
	) {
		//orxLOG("LEFT");
		controlActive = orxTRUE;
		leftWheelPower = -MAX_WHEEL_POWER;
		rightWheelPower = MAX_WHEEL_POWER;
	}
	
	if (orxInput_IsActive("StickRight") &&
		!orxInput_IsActive("StickLeft") &&
		!orxInput_IsActive("StickForward") &&
		!orxInput_IsActive("StickBackwards")
	) {
		//orxLOG("RIGHT");
		controlActive = orxTRUE;
		leftWheelPower = MAX_WHEEL_POWER;
		rightWheelPower = -MAX_WHEEL_POWER;
	}

	if (orxInput_IsActive("StickForward")) {
				//orxLOG("FORWARD");

		controlActive = orxTRUE;
		
		leftWheelPower = HALF_WHEEL_POWER;
		rightWheelPower = HALF_WHEEL_POWER;
		
		if (orxInput_IsActive("StickLeft")){
			leftWheelPower = -HALF_WHEEL_POWER;
			rightWheelPower = MAX_WHEEL_POWER;
		}
		if (orxInput_IsActive("StickRight")){
			leftWheelPower = MAX_WHEEL_POWER;
			rightWheelPower = -HALF_WHEEL_POWER;
		}
/*		if (orxInput_IsActive("StickBackwards")){
			leftWheelPower = HALF_WHEEL_POWER;
			rightWheelPower = MAX_WHEEL_POWER;
		}*/

	}
	
	if (orxInput_IsActive("StickBackwards")) {
		controlActive = orxTRUE;

		leftWheelPower = -HALF_WHEEL_POWER;
		rightWheelPower = -HALF_WHEEL_POWER;
		
		if (orxInput_IsActive("StickRight")){
			//rightWheelPower = HALF_WHEEL_POWER;
			leftWheelPower = -MAX_WHEEL_POWER;
		}
		if (orxInput_IsActive("StickLeft")){
			rightWheelPower = -MAX_WHEEL_POWER;
			//leftWheelPower = HALF_WHEEL_POWER;
		}
/*		if (orxInput_IsActive("StickBackwards")){
			leftWheelPower = -HALF_WHEEL_POWER;
			rightWheelPower = -MAX_WHEEL_POWER;
		}*/

	}


	if (controlActive){
		//orxLOG("controlActive");
			//do left
			orxVECTOR leftLocalInfluencePoint = { -60, 40, 0 };
			BodyThrust(leftLocalInfluencePoint, leftWheelPower );
			
			//do right
			orxVECTOR rightLocalInfluencePoint = { 60, 40, 0 };
			BodyThrust(rightLocalInfluencePoint, rightWheelPower);
	}
	

}

orxBOOL Robot::OnCollide(ScrollObject *_poCollider, 
	const orxSTRING _zPartName, 
	const orxSTRING _zColliderPartName, 
	const orxVECTOR &_rvPosition, 
	const orxVECTOR &_rvNormal)
{
	if (_poCollider == orxNULL) {
		return orxTRUE;
	}

	const orxSTRING colliderName = _poCollider->GetModelName();
	if (
			orxString_SearchString(colliderName, "Shield") != orxNULL ||
			orxString_SearchString(colliderName, "Top") != orxNULL ||
			orxString_SearchString(colliderName, "Right") != orxNULL ||
			orxString_SearchString(colliderName, "Bottom") != orxNULL ||
			orxString_SearchString(colliderName, "Left") != orxNULL ||
			orxString_SearchString(colliderName, "Outpost") != orxNULL
		)
		{

		//orxVECTOR alienPosition = orxVECTOR_0;
		//this->GetPosition(alienPosition, orxTRUE);

	}


	return orxTRUE;
}


